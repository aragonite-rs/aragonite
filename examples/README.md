# Aragonite examples

Currently there are two examples. Ensure you have cargo-aragonite installed:

```
cargo install cargo-aragonite
```

## Linux x64

```
cargo aragonite build --bin linux_x64
```

## Windows

```
cargo aragonite build --bin win_x64
```

#![no_std]
#![no_main]

use aragonite::{aragonite_main, linux::x64::syscall::syscall3};

fn print(buf: &str) -> usize {
    const SYS_WRITE: usize = 1;
    const STDOUT: usize = 1;
    unsafe { syscall3(SYS_WRITE, STDOUT, buf.as_ptr() as usize, buf.len()) }
}

#[aragonite_main(family = "linux")]
pub fn main() {
    print("hello world\n");
}

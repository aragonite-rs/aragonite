#![no_std]
#![no_main]

use aragonite::aragonite_main;
use aragonite::win::x64::pebwalk::get_proc_from_module;

use windows_sys::{
    core::PCSTR,
    Win32::{Foundation::HWND, UI::WindowsAndMessaging::MB_OK},
};

/// The entrypoint for our shellcode.
#[aragonite_main(family = "win")]
pub fn main() -> Option<()> {
    // find the load library function so we can ensure user32.dll exists in the process space
    type LoadLibraryA = extern "win64" fn(PCSTR) -> i32;
    let load_library_a = get_proc_from_module!("kernel32.dll", LoadLibraryA)?;
    load_library_a("user32.dll\0".as_ptr());

    // display a message box
    type MessageBoxA = extern "win64" fn(HWND, PCSTR, PCSTR, u32) -> i32;
    let message_box_a = get_proc_from_module!("user32.dll", MessageBoxA)?;
    message_box_a(0, "It works!\0".as_ptr(), "message box\0".as_ptr(), MB_OK);

    None
}

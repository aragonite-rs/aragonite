#![doc = include_str!("../README.md")]

use std::env;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

pub fn build() {
    let Ok(out) = env::var("OUT_DIR") else {
        panic!("$OUT_DIR variable not found, not running in build context?");
    };
    let linker_path = PathBuf::from(out).join("aragonite.ld");
    let linker_script = include_bytes!("./aragonite.ld");
    let mut linker_file = File::create(&linker_path).expect("not able to create linker script");
    let _ = linker_file.write_all(linker_script);

    println!("cargo::rustc-link-arg=-nostdlib");
    println!("cargo::rustc-link-arg=-nostartfiles");
    println!(
        "cargo::rustc-link-arg=-Wl,-T{path},--build-id=none",
        path = linker_path.to_str().unwrap()
    );
}

use std::collections::HashMap;
use std::env;
use std::path::{Path, PathBuf};
use std::process::Command;

use cargo_metadata::Message;

use colored::Colorize;

pub fn get_project_root() -> std::io::Result<PathBuf> {
    let path = env::current_dir()?;
    let path_ancestors = path.as_path().ancestors();

    for p in path_ancestors {
        let has_cargo = std::fs::read_dir(p)?.any(|p| p.unwrap().file_name() == *"Cargo.toml");
        if has_cargo {
            return Ok(PathBuf::from(p));
        }
    }
    Err(std::io::Error::new(
        std::io::ErrorKind::NotFound,
        "Ran out of places to find Cargo.toml",
    ))
}

/// A local temporary file that auto deletes itself when it leaves scope.
/// `LocalTempFile` will not override any existing files
struct LocalTempFile {
    path: PathBuf,
    exists: bool,
}

impl LocalTempFile {
    fn new(path: PathBuf, contents: &str) -> Option<Self> {
        let exists = path.exists();
        if !exists {
            let Ok(_) = std::fs::write(path.clone(), contents) else {
                return None;
            };
        }
        Some(LocalTempFile { path, exists })
    }
}

impl Drop for LocalTempFile {
    fn drop(&mut self) {
        if self.exists {
            return;
        }
        let _ = std::fs::remove_file(self.path.clone());
    }
}

struct ExitStatus(std::process::ExitStatus);

impl From<std::process::ExitStatus> for ExitStatus {
    fn from(value: std::process::ExitStatus) -> Self {
        Self(value)
    }
}

impl std::process::Termination for ExitStatus {
    fn report(self) -> std::process::ExitCode {
        let code = u8::try_from(self.0.code().unwrap_or(1)).unwrap_or(1);
        std::process::ExitCode::from(code)
    }
}

fn main() -> std::io::Result<ExitStatus> {
    let mut args: Vec<String> = env::args().collect();
    let cargo_env: HashMap<String, String> = env::vars().collect();

    if args.len() < 2 {
        let cargo_args: Vec<String> = args[1..].to_vec();
        let mut command = Command::new("cargo")
            .args(cargo_args)
            .envs(cargo_env)
            .spawn()
            .unwrap();
        return command.wait().map(|ret| ret.into());
    }

    let mut command = args[1].as_str();
    while command == "aragonite" {
        args = args[1..].to_vec();
        command = args[1].as_str();
    }

    match command {
        "build" => {
            use std::io::Write;
            let linker_script = include_bytes!("./aragonite.ld");
            let mut linker_file = tempfile::NamedTempFile::new().unwrap();
            let _ = linker_file.write_all(linker_script);
            let config = include_bytes!("./config.toml");
            let mut config_file = tempfile::NamedTempFile::new().unwrap();
            let _ = config_file.write_all(config);
            let mut build_path = get_project_root()?;
            build_path.push(Path::new("build.rs"));
            let build_rs = format!(
                r#"
                fn main() {{
                    println!("cargo::rustc-link-arg=-static");
                    println!("cargo::rustc-link-arg=-nostdlib");
                    println!("cargo::rustc-link-arg=-nostartfiles");
                    println!("cargo::rustc-link-arg=-Wl,-T{path},--build-id=none");
                }}
            "#,
                path = linker_file.path().to_str().unwrap()
            );
            let _build_rs = LocalTempFile::new(build_path, &build_rs);
            let mut cargo_args = vec![
                "build".to_owned(),
                format!(
                    "--config={path}",
                    path = config_file.path().to_str().unwrap()
                ),
                "--profile=aragonite".to_owned(),
                "--target=x86_64-unknown-linux-gnu".to_owned(),
                "--message-format=json-render-diagnostics".to_owned(),
            ];
            cargo_args.extend(args[2..].iter().cloned());
            let mut command = Command::new("cargo")
                .args(cargo_args)
                .envs(cargo_env)
                .stdout(std::process::Stdio::piped())
                .spawn()
                .unwrap();
            let reader = std::io::BufReader::new(command.stdout.take().unwrap());
            let mut is_finished = false;
            let mut last_artifact = None;
            for message in Message::parse_stream(reader) {
                match message.unwrap() {
                    Message::CompilerArtifact(artifact) => {
                        last_artifact = Some(artifact);
                    }
                    Message::BuildFinished(_) => {
                        is_finished = true;
                    }
                    _ => {}
                }
            }
            let ret = command.wait().map(|ret| ret.into());
            if !is_finished {
                return ret;
            }
            if let Some(last_artifact) = last_artifact {
                let Some(elf_path) = last_artifact.executable else {
                    return ret;
                };
                let elf_file = std::fs::File::open(elf_path.clone())?;
                let reader = std::io::BufReader::new(elf_file);
                let mut elf =
                    elf::ElfStream::<elf::endian::AnyEndian, _>::open_stream(reader).unwrap();
                let mut bin_path = elf_path.to_path_buf();
                bin_path.set_extension("bin");
                let mut bin_file = std::fs::File::create(bin_path.to_path_buf())
                    .expect("file creation in build directory should work");
                let elf_sections = elf.section_headers().clone();
                for section in elf_sections {
                    const SH_TYPE_PROGBITS: u32 = 0x01;
                    if section.sh_type != SH_TYPE_PROGBITS {
                        continue;
                    }
                    let (section_data, _) =
                        elf.section_data(&section).expect("elf file will be valid");
                    let _ = bin_file.write_all(section_data);
                }
                eprintln!("{:>12} {}", "Built".green(), bin_path.as_str());
            }

            ret
        }
        _ => {
            let cargo_args: Vec<String> = args[1..].to_vec();
            let mut command = Command::new("cargo")
                .args(cargo_args)
                .envs(cargo_env)
                .spawn()
                .unwrap();
            command.wait().map(|ret| ret.into())
        }
    }
}

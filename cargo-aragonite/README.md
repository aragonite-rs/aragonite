# cargo-aragonite

`cargo-aragonite` is a cargo extension for building aragonite projects as .bin files for use as position-independent shellcode.

It is a simple wrapper around `cargo` that sets up extra arguments when it detects the `build` command. All other arguments are passed transparently to the underlying `cargo` process.

## Installation

```
cargo install cargo-aragonite
```

Ensure your `$PATH` variable is setup to use cargo install binaries correctly.

## Usage

The basic usage is:

```
cargo aragonite build [any other arguments here]
```

### Example

The examples in this workspace can be built using:

```
cargo aragonite build -p examples --bin linux_x64
```

or


```
cargo aragonite build -p examples --bin win_x64
```

***Note: at this time, `cargo-aragonite` only supports building one .bin at a time***


## Details

The process that `cargo-aragonite` provides is as follows:

- sets up a cargo profile `aragonite` optimized with space (`z`)
- creates a linker script that removes irrelevant code and orders the resulting functions correctly
- creates a build script to set up extra linker arguments to ensure that the produced file is a statically linked file without external dependencies
- parses the resulting executable's sections so that only the program code is left and saves that to a .bin file

## Using in workspaces, or using with additional build scripts

Because `cargo-aragonite` needs to use a rust build script to build arguments, `cargo-aragonite` can also be used as a build-dependency for more complex workflows. Add cargo-aragonite as a build dependency:

```Cargo.toml
# Cargo.toml

[build-dependencies]
cargo-aragonite = "0.1.0"
```

And then use the `build` function to do the necessary setup:

```rust
// build.rs

fn main() {
    cargo_aragonite::build();

    // any other build steps as required
}
```

Please note, that you will still need to use `cargo aragonite build` to perform the rest of the steps as outlined above.

Also note, that for workspaces this is not strictly necessary, but without it, your current working directory must be within the specific workspace member that you are trying to build (rather than, using `cargo aragonite build -p [project name]` from anywhere within the workspace).

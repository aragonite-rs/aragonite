# aragonite_macros

This crate contains the definition for the following macros:

- `aragonite_main`

For use with the aragonite shellcode generation framework:

https://www.gitlab.com/aragonite-rs/aragonite

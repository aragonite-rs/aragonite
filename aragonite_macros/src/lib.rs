use proc_macro::TokenStream;
use quote::{format_ident, quote};
use syn::{parse_macro_input, punctuated::Punctuated};

enum TargetFamily {
    Unix,
    Windows,
}

enum TargetArch {
    X64,
}

struct Options {
    panic: bool,
    cleanup: bool,
    target_family: Option<TargetFamily>,
    target_arch: TargetArch,
}

impl Default for Options {
    fn default() -> Self {
        Options {
            panic: true,
            cleanup: true,
            target_family: None,
            target_arch: TargetArch::X64,
        }
    }
}

/// aragonite_main
///
/// Arguments:
///   family = "VALUE" which argument family to generate safe cleanup code for
///                     supported families:
///                       - win
///                       - linux
///   arch = "VALUE"   which architecture to generate safe cleanup code for
///                     supported architectures:
///                       - x64 (default)
///   no_cleanup       do not automatically generate cleanup code
///   no_panic         do not automatically generate a panic handler
///
#[proc_macro_attribute]
pub fn aragonite_main(args: TokenStream, input: TokenStream) -> TokenStream {
    let mut func = parse_macro_input!(input as syn::ItemFn);
    let new_ident = format_ident!("__{ident}", ident = func.sig.ident);
    func.sig.ident = new_ident.clone();

    let args =
        parse_macro_input!(args with Punctuated<syn::Meta, syn::Token![,]>::parse_terminated);

    let mut options = Options::default();
    for arg in args {
        match &arg {
            syn::Meta::Path(path) => {
                let last = path.segments.last().expect("there is at least one segment");
                if last.ident == syn::Ident::new("no_panic", proc_macro2::Span::call_site()) {
                    options.panic = false
                } else if last.ident
                    == syn::Ident::new("no_cleanup", proc_macro2::Span::call_site())
                {
                    options.cleanup = false
                } else {
                    panic!("unknown attribute: {}", last.ident);
                }
            }
            syn::Meta::NameValue(syn::MetaNameValue { path, value, .. }) => {
                let last = path.segments.last().expect("there is at least one segment");
                let value = match value {
                    syn::Expr::Lit(lit) => match &lit.lit {
                        syn::Lit::Str(s) => s.value(),
                        _ => panic!("unsupported literal value"),
                    },
                    _ => panic!("unsupported value"),
                };
                if last.ident == syn::Ident::new("family", proc_macro2::Span::call_site()) {
                    match value.as_str() {
                        "w" | "win" | "windows" => {
                            options.target_family = Some(TargetFamily::Windows);
                        }
                        "l" | "lin" | "linux" | "unix" => {
                            options.target_family = Some(TargetFamily::Unix);
                        }
                        _ => panic!("unsupported target family: {}", value),
                    }
                } else if last.ident == syn::Ident::new("arch", proc_macro2::Span::call_site()) {
                    match value.as_str() {
                        "x64" | "x86_64" | "amd64" => {
                            options.target_arch = TargetArch::X64;
                        }
                        _ => panic!("unsupported target arch: {}", value),
                    }
                } else {
                    panic!("unknown attribute: {}", last.ident);
                }
            }
            _ => {}
        }
    }

    let main_fn = if options.cleanup {
        match options.target_family {
            Some(TargetFamily::Windows) => match options.target_arch {
                TargetArch::X64 => {
                    quote! {
                    #[no_mangle]
                    #[link_section = ".aragonite_main"]
                    pub fn main() {
                        type ExitProcess = extern "win64" fn(u32);
                        let Some(__aragonite__macro__exit_process) = ::aragonite::win::x64::pebwalk::get_proc_from_module!(
                            "kernel32.dll",
                            ExitProcess
                        ) else {
                            return;
                        };

                        #func
                        #new_ident();

                        __aragonite__macro__exit_process(0);
                        }
                    }
                }
            },
            Some(TargetFamily::Unix) => match options.target_arch {
                TargetArch::X64 => {
                    quote! {
                        #[no_mangle]
                        #[link_section = ".aragonite_main"]
                        pub fn main() {
                            #func
                            #new_ident();
                            unsafe { ::aragonite::linux::x64::syscall::syscall1(60, 0usize); }
                        }
                    }
                }
            },
            None => {
                quote! {
                    #[no_mangle]
                    #[link_section = ".aragonite_main"]
                    pub fn main() {
                        #func
                        #new_ident();
                    }
                }
            }
        }
    } else {
        quote! {
            #[no_mangle]
            #[link_section = ".aragonite_main"]
            pub fn main() {
                #func
                #new_ident();
            }
        }
    };

    let panic_stub = if options.panic {
        quote! {
            #[panic_handler]
            fn panic(_: &core::panic::PanicInfo) -> ! {
                loop {}
            }
        }
    } else {
        quote!()
    };

    quote! {
        #panic_stub
        #main_fn
    }
    .into()
}
